# Data display

Data display for LiberaForms developed with https://vue-loader.vuejs.org/

## Why

LiberaForms collects data solicited via forms. It is not easy or practical to display such data with **tables** because the number of columns and the text length of the column headers and fields are unknown. This Vue component displays data in various ways (modes). It is used to display:

* The list of a user's forms (My forms)
* The answers to each individual form
* The list of all forms (admin)
* The list of all users (admin)
* A user's forms (admin)

## The Panel

Data display begins with a `control panel` that includes functions according to the data being displayed.

* Export and links to other visualizations
* Filters. Search, Order by, Ascending.
* Column order. Move any column to the first column position.
* Modes. Grid, Expanded, Cards, Edit mode
* Pagination

## Modes

Modes provide different ways to visualize the same data

### Grid (table)
* Each row begins with 'control column' that includes a `mark` button, and an `edit` button (if fields are editable).
* Column widths are evenly spaced if they all fit within the table width, and if not...
* Column widths are evenly spaced and the first column becomes `sticky`. Cell content is ellipsed, and table overflow is hidden.

### Expanded (cards)

The Expanded mode displays each row as a card. The title of the card is the Grid's control column and first data column, but also include an expand/collapse button.

### Small Cards

Rendered when small screens are detected. This mode is the same as the Expanded mode except that cards are initially collapsed, and the panel can be toggled.

## Data inspection

Every field is clickable. A modal is displayed containing the entire field value. If the field is editable it can be edited via a form element. The form is built using the same element (select, textarea, etc) as initially defined with the LiberaForms' Form builder.


## User preferences

Defining filters, data order and column order makes consulting the data easier. These preferences are saved to the database and recovered when the user returns to read that same data.

## Exports

Current form answers can be exported as json. Other export options are done on the server. Our intention is to move those options into this component.

## Usage

```
<link rel="stylesheet" href="/static/data-display/data-display.css">

<div data-vue-component="data-display"
     data-endpoint="/data-display/my-forms/{{g.current_user.id}}"
     data-language="{{ g.current_user.language }}"
     data-csrf_token="{{ csrf_token() }}">
</div>

<script src="/static/data-display/data-display.js"></script>
```

# Development

```
npm run build
```

We then copy the generated `data-display.js` file to the `/static/data-display/` directory of LiberaForms server project.

You may also wish to read the backend code at `/liberaforms/views/data_display.py`
