# Licenses

`Data display` for LiberaForms is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE version 3 or later.

Included in this directory is a copy of that license.

A copy of the MIT license is also included here and corresponds to the libraries listed in the NOTICE file.
