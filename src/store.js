/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import _ from 'underscore';

Vue.use(Vuex)

const store = new Vuex.Store({
  debug: true,
  state: {
    downloading: true,
    endpoint: null,
    item_endpoint: null,
    items: [],
    filtered_items: null,
    deleted_fields: [],
    default_field_index: [],
    meta: {'ascending': true},
    user_prefs: {},
    page: 1,
    page_length: 10,
    can_edit: false,
    has_row_controls: false,
    include_deleted_fields: false,
    expanded_grid: false,
    pdf_builder_mode: false,
  },
  mutations: {
    setEndpoint(state, endpoint) {
      state.endpoint = endpoint
    },
    setMeta(state, meta) {
      state.meta = meta
      state.deleted_fields = meta.deleted_fields
      state.item_endpoint = meta.item_endpoint
      state.can_edit = meta.can_edit
      state.enable_exports = meta.enable_exports
      state.default_field_index = meta.default_field_index
    },
    setUserPrefs(state, prefs) {
      state.user_prefs = prefs
    },
    setFieldIndex(state, field_index) {
      state.user_prefs.field_index = field_index
    },
    setItems (state, items) {
      state.items = items
    },
    setFilteredItems (state, items) {
      state.filtered_items = items
    },
    removeItem(state, item_id) {
      let item_pos = state.items.findIndex(item => item.id === item_id)
      state.items.splice(item_pos, 1)
      state.meta.total = state.meta.total - 1
    },
    markItem (state, payload) {
      let item = state.items.find(i => i.id == payload.id);
      if (item) {
        item.marked = item.marked ? false : true
        axios.post(payload.endpoint + '/mark')
          .then(function(response) {
            item.marked = response.data.marked
          })
          .catch(e => {
            item.marked = item.marked ? false : true
            console.log(e)
          })
      }
    },
    setItemData(state, payload) {
      var item = state.items.find(item => item.id === payload.item_id)
      if (item !== undefined) {
        item.data = payload.item_data
      }
    },
    setPage(state, page) {
      state.page = page
    },
    setOrderBy(state, order_by) {
      state.user_prefs.order_by = order_by
      state.items = order_items(state.items,
                                state.user_prefs.order_by,
                                state.user_prefs.ascending)
      axios.post(state.endpoint + '/order-by-field', {
        order_by_field_name: order_by
      })
      .then(function (response) {
        //self.setOrderBy(response.data.order_by_field_name)
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    setAscending(state, ascending) {
      state.user_prefs.ascending = ascending
      state.items = order_items(state.items,
                                state.user_prefs.order_by,
                                state.user_prefs.ascending)
      //state.items = state.items.reverse()
      if (state.filtered_items !== null) {
        state.filtered_items = order_items(state.filtered_items,
                                           state.user_prefs.order_by,
                                           state.user_prefs.ascending)
        //state.filtered_items = state.filtered_items.reverse()
      }
      axios.post(state.endpoint + '/toggle-ascending')
      .then(function (response) {
        //console.log("response.data.ascending: " +response.data.ascending)
        //self.setAscending(response.data.ascending)
      })
      .catch(function (error) {
        console.log(error);
      });
    },
    setIncludeDeletedFields(state, include) {
      state.include_deleted_fields = include
      state.filtered_items = null
    },
    toggleExpandedView(state) {
      state.expanded_grid = state.expanded_grid ? false : true
    },
    togglePdfBuilderMode(state) {
      state.pdf_builder_mode = state.pdf_builder_mode ? false : true
    },
  },
  getters: {
    isDownloading: state => {
      return state.downloading
    },
    getEndpoint: state => {
      return state.endpoint
    },
    getItemEndpoint: state => {
      return state.item_endpoint
    },
    getCanEdit: state => {
      return state.items.length == 0 ? false : state.can_edit
    },
    getExportsEnabled: state => {
      return state.meta.enable_exports
    },
    getGraphsEnabled: state => {
      return state.meta.enable_graphs
    },
    getNotificationsEnabled: state => {
      return state.meta.enable_notification
    },
    getAllItems: state => {
      return state.items
    },
    getItemsToDisplay: state => {
      return state.filtered_items !== null ? state.filtered_items : state.items
    },
    getPage: state => {
      return state.page
    },
    getPageLength: state => {
      return state.page_length
    },
    getPaginatedItems: (state, getters) => {
      var slice_start = (state.page - 1) * state.page_length
      var items_to_display = getters.getItemsToDisplay
      return items_to_display.slice(slice_start, slice_start + state.page_length)
    },
    getDisplayingItemsMessage: (state, getters) => {
      var slice_start = (state.page - 1) * state.page
      var total_items = getters.getItemsToDisplay.length
      if (total_items < state.page_length) {
        return "Displaying "+total_items+" "+"of"+" "+total_items
      }
      var last_item = state.page * state.page_length
      last_item = last_item > total_items ? total_items : last_item
      var first_item = ((state.page -1) * state.page_length) + 1
      return "Displaying "+first_item+" - "+last_item+" "+"of"+" "+total_items
    },
    getItemById: (state) => (item_id) => {
      return state.items.find(item => item.id === item_id)
    },
    getFieldIndex: (state) => {
      if (state.include_deleted_fields) {
        return state.user_prefs.field_index.concat(state.deleted_fields)
      } else {
        return state.user_prefs.field_index
      }
    },
    getDefaultFieldIndex: (state) => {
      return state.default_field_index
    },
    getDeletedFields: (state) => {
      //console.log("state.deleted_fields: "+state.deleted_fields)
      return state.deleted_fields
    },
    getIsDeletedField: (state) => (field_name) => {
      if (state.deleted_fields.find(x => x.name === field_name)) {
        return true
      } else {
        return false
      }
    },
    getFieldLabel: (state) => (field_name) => {
      var field = state.user_prefs.field_index.find(x => x.name === field_name)
      if (field !== undefined) {
        return field.label
      }
      return null
    },
    getOptionLabel: (state, getters) => (payload) => {
      var field_name = payload.field_name
      var option_value = payload.option_value
      var field = getters.getFieldStructure(field_name)
      var label = option_value
      if (field === undefined) {
        return label
      }
      field.values.forEach((value) => {
        if (value.value == option_value) {
          label = value.label
        }
      });
      return label
    },
    getFirstField: (state) => {
      if (state.user_prefs.field_index[0].name != 'marked') {
        return state.user_prefs.field_index[0]
      } else {
        return state.user_prefs.field_index[1]
      }
    },
    getFieldStructure: (state) => (field_name) => {
      return _.findWhere(state.meta.form_structure, {name: field_name})
    },
    getOrderBy: (state) => {
      return state.user_prefs.order_by
    },
    getAscending: (state) => {
      return state.user_prefs.ascending
    },
    getExpandedViewState: (state) => {
      return state.expanded_grid
    },
    getPdfBuilderMode: (state) => {
      return state.pdf_builder_mode
    },
    getHasRowControls: (state) => {
      return state.has_row_controls
    },
    getIncludeDeletedFields: (state) => {
      return state.include_deleted_fields
    },
    getName: (state) => {
      return state.meta.name
    },
    getFormattedValue: (state, getters) => (payload) => {
      var field_name = payload.field_name
      var field_value = payload.field_value
      if (field_name == 'created' || field_name == 'marked') {
        return
      }
      if (field_name.startsWith('checkbox') ||
          field_name.startsWith('radio-group') ||
          field_name.startsWith('select')){
        var result = []
        var values = field_value.split(', ')
        values.forEach((value) => {
          var label = getters.getOptionLabel({'field_name': field_name,
                                              'option_value': value})
          result.push(label)
        });
        if (result.length > 0) {
          return result.join(', ')
        }
      }
      return field_value
    },
  },
  actions: {
    async downloadItems(context) {
      await axios.get(context.state.endpoint).then(response => {
              context.commit('setMeta', response.data.meta)
              context.commit('setUserPrefs', response.data.user_prefs)
              var items = order_items(response.data.items,
                                      context.state.user_prefs.order_by,
                                      context.state.user_prefs.ascending)
              context.commit('setItems', items)
              if ((context.state.items.length > 0 && context.state.items[0].marked !== undefined)
                  || context.state.can_edit) {
                context.state.has_row_controls = true
              }
            })
            .catch(e => {
              console.log(e)
            })
            .finally(() => {
              context.state.downloading = false
            });
    },
    filterItems(context, filter_text) {
      if (filter_text === "") {
        context.commit('setFilteredItems', null)
        return
      }
      filter_text = filter_text.toLowerCase()
      var result = _.filter(
                        context.state.items,
                        function(item) {
                          if (item.created.indexOf(filter_text) !== -1) {
                            return item
                          }
                          for (var field_name in item.data){
                            var field_value = item.data[field_name]
                            if (field_value.value !== undefined) {
                              field_value = field_value.value
                            }
                            var field_value = String(field_value).toLowerCase()
                            if (field_value.indexOf(filter_text) !== -1) {
                              return item
                            }
                          }
                        });
      context.commit('setFilteredItems', result)
    },
    saveItemData(context, payload) {
      var item_id = payload.item_id
      var item = context.state.items.find(item => item.id === item_id)
      if (item === undefined) {
        return
      }
      item.data[payload.field_name] = payload.field_value

      var json = JSON.stringify({item_data: item.data});
      axios.patch(context.state.item_endpoint+item_id+'/save', json, {
                   headers: { 'Content-Type': 'application/json' }
        })
        .then(response => {
          if (response.data.saved == true) {
            item.data = response.data.data
          }
        })
        .catch(e => {
          console.log(e)
        });
    },
    setOrder(context, field_name) {
      // called when column header is clicked
      if (context.state.user_prefs.order_by != field_name) {
        context.commit('setOrderBy', field_name)
      } else {
        var asc = context.state.user_prefs.ascending ? false : true
        context.commit('setAscending', asc)
      }
    },
  },
})

function order_items(items, field_name, ascending) {
  if (field_name == 'created') {
    items = _.sortBy(items, field_name)
  } else {
    items.sort(function compareFn(first_el, second_el) {
      if (field_name.endsWith('__html')) {
        var first_data = first_el.data[field_name].value
        var second_data = second_el.data[field_name].value
      } else {
        var first_data = first_el.data[field_name]
        var second_data = second_el.data[field_name]
      }
      if (first_data === undefined || !first_data) {
        return -1;
      }
      if (second_data === undefined) {
        return 1;
      }
      if (isNaN(first_data)) {
        first_data = first_data.toLowerCase()
        second_data = second_data.toLowerCase()
      } else {
        first_data = parseInt(first_data)
        second_data = parseInt(second_data)
      }
      if (first_data < second_data) {
        return -1;
      }
      return 1;
    })
  }
  return ascending == true ? items : items.reverse()
}

export default store
