/*
This file is part of LiberaForms.

# SPDX-FileCopyrightText: 2021 LiberaForms.org
# SPDX-License-Identifier: AGPL-3.0-or-later
*/

import Vue from "vue";
import VueMq from 'vue-mq'
import DataDisplay from "../components/dataDisplay.vue";

import 'es6-promise/auto'
import store from "../store.js"
import i18n from "../i18n.js"
export const bus = new Vue();

var ComponentClass = Vue.extend(DataDisplay)
                        .use(VueMq, {
                          breakpoints: { // default breakpoints - customize this
                            sm: 450,
                            md: 1250,
                            lg: Infinity,
                          },
                          defaultBreakpoint: 'sm' // customize this for SSR
                        })

document.querySelectorAll("[data-vue-component=data-display]")
        .forEach((element) => {
          new ComponentClass({
            propsData: { ...element.dataset },
            props: ["endpoint", "language", "csrf_token"],
            components: {'DataDisplay': DataDisplay},
            store: store,
            i18n: i18n,
          }).$mount(element);
  });
